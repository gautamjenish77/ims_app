# ims_app

**Fertilizer management system**

_This mobile application is used for entering the sales data of commodity to the farmers of different regions._

---
## Uses of languages:
- Dart.

**Dependencies:**
``` Flutter

name: IMS

version: 1.0.0+1

environment:
  sdk: ">=2.1.0 <3.0.0"

 dependencies:
   flutter:
     sdk: flutter
   splashscreen: ^1.2.0
   simple_animations: ^1.3.3
   page_transition: ^1.1.4
   google_fonts: ^0.3.2
   dropdownfield: ^1.0.3
   form_field_validator: ^1.0.1
   http: ^0.12.2
   shared_preferences: ^0.5.10
   flushbar: ^1.10.4

   # The following adds the Cupertino Icons font to your application.
   # Use with the CupertinoIcons class for iOS style icons.
   cupertino_icons: ^0.1.3
   flutter_svg: ^0.16.0
   flutter_offline: ^0.3.0
   connectivity: ^0.4.9+2
   flutter_offline: ^0.3.0
   connectivity: ^0.4.9+2
 
 dev_dependencies:
   flutter_launcher_icons: ^0.7.5
   flutter_test:
     sdk: flutter
   flutter_launcher_icons: ^0.7.5
   form_field_validator: ^1.0.1

flutter_icons:
  android: true
  ios: true
  image_path: "assets/white.png"

fonts:
    - family: PTSans
      fonts:
        - asset: assets/fonts/PTSansNarrow-Regular.ttf
          weight: 300
        - asset: assets/fonts/PTSansNarrow-Bold.ttf
          weight: 600

```
## Developed by: 
![IMS](https://rarait.com/wp-content/uploads/2019/08/Final-logo-200px-.png)
[_Rara Information Technology_](https://rarait.com/)

> Hope it made you easier