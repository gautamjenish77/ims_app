import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class DatabaseHelper {
  String serverUrl = "http://ims.rarait.com/api";

  var status;

  var token;

  void addData(
    String bag,
    String quantity,
    String bank_id,
    String purpose,
    String notes,
    String action_date,
    String user_id,
    String item,
    String consumer_name,
    String consumer_phone,
    String consumer_address_id,
    String consumer_citizen_id,
    String consumer_area,
  ) async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'token';
    final value = prefs.get(key) ?? 0;

    String myUrl = "http://ims.rarait.com/api/sales";
    http.post(myUrl, headers: {
      'Accept': 'application/json',
      'Authorization':
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZmFlZDFiYjc5Y2I5Zjk1MGU5ZDczNjA3YmNlOGNmMWZlZDg4MTRmMjY1NjZjOWE4NWJlZWRjZjU0NzI3MTllYTE5MTc2NGM5NTQxN2UyYjUiLCJpYXQiOjE2MDY4MDI2OTQsIm5iZiI6MTYwNjgwMjY5NCwiZXhwIjoxNjM4MzM4Njk0LCJzdWIiOiI2NjAiLCJzY29wZXMiOltdfQ.r8L2PON9Hh1HTzANEGkY8JXDWNKg-biFQqEcJGGNUS1Uh6yWAOIFRx2-LiL1nQZkHKXj0zfxet0TEPrY07FdSFA_7QplSwGZdfM11_zdTsTwlHeY_VMJ_2z610UcRz8wNvnQukCPqDPeUkmxqW7dN0oCpmQq9p_ER4QlbTHF77AE5QezfqbZL1OXPyPhftbF1NFA2Y3cYvO38dl37_ypQL6FcnwGKp6Yj99ErkjVeiLUoZQ5cVbKjCjiNNzUpfw3CIN0rly206UZqiy04P44G5M6gMDIG5Ibqsl84GnHu-X562QvMsgiD3fd9Y5fr4MkC797zi4-aFqWdlm2okcjPMZ4TK7jzh9K5_2mnZ70JZ6SwUdUO7r1W6RsDhZafJdfPQ4wdYROwx_VxBiFLP_p2_xa3ZDW1O2AC12Kn3zeOP_ENVgmh7icAX71GRMzSHsYufzJaANNr24u2VqXHD0Wjh9qmtm_sVvZ-_-C94re24hTmzs71nZNPvzPPGgvQrHIxe9tEZwktU8e-C-rglYfdc_q6SlXGpT8SiU6raJhQRLAkX4aq-55c3r6TV4fEHWuMdjZHa2ZBgWla-kRLUa188Ae41q8z9OL4xb3Yrb2N54Vc4WOzWreQs_0j7StT7fPUXBZvAYeS0wQqtSyQpSokfFKSIkTPZwnKHIdKqjng8Q'
    }, body: {
      "bag": "$bag",
      "quantity": "$quantity",
      "bank_id": "$bank_id",
      "purpose": "$purpose",
      "notes": "$notes",
      "action_date": "$action_date",
      "user_id": "$user_id",
      "item": "$item",
      "consumer_name": "$consumer_name",
      "consumer_phone": "$consumer_phone",
      "consumer_address_id": "$consumer_address_id",
      "consumer_citizen_id": "$consumer_citizen_id",
      "consumer_area": "$consumer_area"
    }).then((response) {
      print('Response status : ${response.statusCode}');
      print('Response body : ${response.body}');
    });
  }

  Future<Map<String, dynamic>> getData() async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'token';
    final value = prefs.get(key) ?? 0;

    String myUrl = "$serverUrl/sales";
    http.Response response = await http.get(myUrl, headers: {
      'Accept': 'application/json',
      'Authorization':
          'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZmFlZDFiYjc5Y2I5Zjk1MGU5ZDczNjA3YmNlOGNmMWZlZDg4MTRmMjY1NjZjOWE4NWJlZWRjZjU0NzI3MTllYTE5MTc2NGM5NTQxN2UyYjUiLCJpYXQiOjE2MDY4MDI2OTQsIm5iZiI6MTYwNjgwMjY5NCwiZXhwIjoxNjM4MzM4Njk0LCJzdWIiOiI2NjAiLCJzY29wZXMiOltdfQ.r8L2PON9Hh1HTzANEGkY8JXDWNKg-biFQqEcJGGNUS1Uh6yWAOIFRx2-LiL1nQZkHKXj0zfxet0TEPrY07FdSFA_7QplSwGZdfM11_zdTsTwlHeY_VMJ_2z610UcRz8wNvnQukCPqDPeUkmxqW7dN0oCpmQq9p_ER4QlbTHF77AE5QezfqbZL1OXPyPhftbF1NFA2Y3cYvO38dl37_ypQL6FcnwGKp6Yj99ErkjVeiLUoZQ5cVbKjCjiNNzUpfw3CIN0rly206UZqiy04P44G5M6gMDIG5Ibqsl84GnHu-X562QvMsgiD3fd9Y5fr4MkC797zi4-aFqWdlm2okcjPMZ4TK7jzh9K5_2mnZ70JZ6SwUdUO7r1W6RsDhZafJdfPQ4wdYROwx_VxBiFLP_p2_xa3ZDW1O2AC12Kn3zeOP_ENVgmh7icAX71GRMzSHsYufzJaANNr24u2VqXHD0Wjh9qmtm_sVvZ-_-C94re24hTmzs71nZNPvzPPGgvQrHIxe9tEZwktU8e-C-rglYfdc_q6SlXGpT8SiU6raJhQRLAkX4aq-55c3r6TV4fEHWuMdjZHa2ZBgWla-kRLUa188Ae41q8z9OL4xb3Yrb2N54Vc4WOzWreQs_0j7StT7fPUXBZvAYeS0wQqtSyQpSokfFKSIkTPZwnKHIdKqjng8Q'
    });
    return json.decode(response.body);
  }

  _save(String token) async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'token';
    final value = token;
    prefs.setString(key, value);
  }

  read() async {
    final prefs = await SharedPreferences.getInstance();
    final key = 'token';
    final value = prefs.get(key) ?? 0;
    print('read : $value');
  }
}
