import 'package:flutter/material.dart';

const LargeTextSize = 26.0;
const MediumTextSize = 20.0;
const BodyTextSize = 18.0;

const String FontNameDefault = 'PTSans';

const AppBarTextStyle = TextStyle(
  fontFamily: FontNameDefault,
  fontSize: MediumTextSize,
  color: Colors.white,
);

const TitleTextStyle = TextStyle(
  fontFamily: FontNameDefault,
  fontSize: LargeTextSize,
  color: Colors.black,
);

const Body1TextStyle = TextStyle(
  fontFamily: FontNameDefault,
  fontSize: BodyTextSize,
  color: Colors.black,
);
