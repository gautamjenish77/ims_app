import 'package:flutter/material.dart';
import 'package:IMS/Controller/ApiHelper.dart';
import 'package:flutter/services.dart';
import 'package:IMS/pages/app_main.dart';

class AddData extends StatefulWidget {
  AddData({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _AddDataState createState() => _AddDataState();
}

class _AddDataState extends State<AddData> {
  bool rice = false;
  bool wheat = false;
  bool maize = false;
  bool mustard = false;
  bool others = false;

  void onChangedRice(bool value) {
    setState(() {
      rice = value;
    });
  }

  void onChangedWheat(bool value) {
    setState(() {
      wheat = value;
    });
  }

  void onChangedMaize(bool value) {
    setState(() {
      maize = value;
    });
  }

  void onChangedMustard(bool value) {
    setState(() {
      mustard = value;
    });
  }

  void onChangedOthers(bool value) {
    setState(() {
      others = value;
    });
  }

  DatabaseHelper databaseHelper = new DatabaseHelper();

  final TextEditingController _bagController = new TextEditingController();

  final TextEditingController _quantityController = new TextEditingController();
  final TextEditingController _bank_idController = new TextEditingController();
  final TextEditingController _purposeController = new TextEditingController();
  final TextEditingController _notesController = new TextEditingController();
  final TextEditingController _action_dateController =
      new TextEditingController();
  final TextEditingController _user_idController = new TextEditingController();
  final TextEditingController _itemController = new TextEditingController();
  final TextEditingController _consumer_nameController =
      new TextEditingController();
  final TextEditingController _consumer_phoneController =
      new TextEditingController();
  final TextEditingController _consumer_address_idController =
      new TextEditingController();
  final TextEditingController _consumer_citizen_idController =
      new TextEditingController();
  final TextEditingController _consumer_areaController =
      new TextEditingController();

  GlobalKey<FormState> salesformkey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Add Sales",
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.home,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => new FirstPage()));
            },
          ),
          backgroundColor: Color(0xFF1B5E20),
          title: Text('Add Sales'),
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(vertical: 25.0, horizontal: 27.0),
          child: Form(
            key: salesformkey,
            child: SingleChildScrollView(
              child: Column(
                children: [
                  TextFormField(
                    controller: _bagController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'No. of Bags',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    controller: _quantityController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Quantity in Metric Ton (M.T)',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _bank_idController,
                    cursorColor: Colors.indigo,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Bank ID / Managable ID',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    child: Text(
                      'Purpose',
                      style: TextStyle(
                        color: Colors.green[900],
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: rice,
                      onChanged: onChangedRice,
                      activeColor: Colors.green[900],
                      title: const Text("Rice"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: wheat,
                      onChanged: onChangedWheat,
                      activeColor: Colors.green[900],
                      title: const Text("Wheat"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: maize,
                      onChanged: onChangedMaize,
                      activeColor: Colors.green[900],
                      title: const Text("Maize"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: mustard,
                      onChanged: onChangedMustard,
                      activeColor: Colors.green[900],
                      title: const Text("Mustard"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0),
                      color: Colors.lightGreen.withOpacity(.2),
                    ),
                    child: CheckboxListTile(
                      value: others,
                      onChanged: onChangedOthers,
                      activeColor: Colors.green[900],
                      title: const Text("Others"),
                      secondary: new Icon(Icons.add_circle),
                    ),
                  ),
                  /*TextFormField(
                    controller: _purposeController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Purpose',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),*/
                  SizedBox(
                    height: 20.0,
                  ),
                  TextFormField(
                    controller: _notesController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Notes',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _action_dateController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Date',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _user_idController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: "User's ID",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _itemController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Items ID (1/2/3/4)',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _consumer_nameController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: "Consumer's Name",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _consumer_phoneController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: "Consumer's Phone No.",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _consumer_address_idController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: "Consumer's Address ID",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _consumer_citizen_idController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: "Consumer's Citizen No.",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    controller: _consumer_areaController,
                    cursorColor: Colors.green,
                    //validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: "Consumer's Area Code",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    height: 50,
                    child: new RaisedButton(
                      onPressed: () {
                        databaseHelper.addData(
                            _bagController.text.trim(),
                            _quantityController.text.trim(),
                            _bank_idController.text.trim(),
                            _purposeController.text.trim(),
                            _notesController.text.trim(),
                            _action_dateController.text.trim(),
                            _user_idController.text.trim(),
                            _itemController.text.trim(),
                            _consumer_nameController.text.trim(),
                            _consumer_phoneController.text.trim(),
                            _consumer_address_idController.text.trim(),
                            _consumer_citizen_idController.text.trim(),
                            _consumer_areaController.text.trim());
                      },
                      color: Colors.green,
                      child: new Text(
                        "Add",
                        style: new TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
