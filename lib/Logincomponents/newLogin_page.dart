import 'dart:convert';

import 'package:IMS/pages/app_main.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flushbar/flushbar_helper.dart';
import 'package:flushbar/flushbar.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:IMS/Logincomponents/forgotpass.dart';
import 'package:google_fonts/google_fonts.dart';

class LoginPage1 extends StatefulWidget {
  @override
  _LoginPage1State createState() => _LoginPage1State();
}

class _LoginPage1State extends State<LoginPage1> {
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passController = TextEditingController();

  bool _isLoading = false;

  //We need the function from the server API
  signIn(String email, String pass) async {
    String url = "http://ims.rarait.com/api/user-login";
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map body = {"email": email, "password": pass};
    var jsonResponse;
    var res = await http.post(url, body: body);
    //Need to check the api status
    if (res.statusCode == 200) {
      jsonResponse = json.decode(res.body);

      print("Response status: ${res.statusCode}");

      print("Response status: ${res.body}");

      if (jsonResponse != null) {
        setState(() {
          _isLoading = false;
        });

        sharedPreferences.setString("key", jsonResponse['token']);

        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (BuildContext context) => FirstPage()),
            (Route<dynamic> route) => false);
      }
    } else {
      setState(() {
        _isLoading = false;
      });
      _showFloatingFlushbar();
      print("Response status : ${res.body}");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(0),
          child: SizedBox(
            width: double.infinity,
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.center,
              //crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 258,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/images/AG.png'),
                      fit: BoxFit.fill,
                    ),
                  ),
                  child: Stack(
                    children: [
                      Positioned(
                        left: 142,
                        width: 150,
                        height: 400,
                        child: Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage('assets/images/blue.png'),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "Welcome Sellers",
                  style: TextStyle(
                    //color: Color(0xFF1B5E20),
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "Sign in with your Seller's Email and Password",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.blueGrey,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                  height: 190,
                  width: MediaQuery.of(context).size.width,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(20)),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            autovalidate: true,
                            controller: _emailController,
                            decoration: InputDecoration(
                              labelText: "Email",
                              hintText: "Enter your seller\'s email",
                              floatingLabelBehavior:
                                  FloatingLabelBehavior.always,
                              suffixIcon: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(0, 20, 20, 20),
                                /*child: SvgPicture.asset(
                                  "icons/Mail.svg",
                                  height: 18.0,
                                  color: Colors.indigo,
                                ),*/
                                child: Icon(
                                  Icons.email,
                                  color: Color(0xFF1B5E20),
                                ),
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                horizontal: 30,
                                vertical: 20,
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15),
                                borderSide: BorderSide(color: Colors.blueGrey),
                                gapPadding: 10,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15),
                                borderSide: BorderSide(color: Colors.grey),
                                gapPadding: 10,
                              ),
                            ),
                            validator:
                                EmailValidator(errorText: "Not a Valid Email"),
                          ),
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            autovalidate: true,
                            controller: _passController,
                            obscureText: true,
                            decoration: InputDecoration(
                              labelText: "Password",
                              hintText: "Enter your password",
                              floatingLabelBehavior:
                                  FloatingLabelBehavior.always,
                              suffixIcon: Padding(
                                padding:
                                    const EdgeInsets.fromLTRB(0, 20, 20, 20),
                                /*child: SvgPicture.asset(
                                  "icons/lock.svg",
                                  height: 20.0,
                                ),*/
                                child: Icon(
                                  Icons.lock,
                                  color: Color(0xFF1B5E20),
                                ),
                              ),
                              contentPadding: EdgeInsets.symmetric(
                                horizontal: 30,
                                vertical: 20,
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15),
                                borderSide: BorderSide(color: Colors.blueGrey),
                                gapPadding: 10,
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(15),
                                borderSide: BorderSide(color: Colors.grey),
                                gapPadding: 10,
                              ),
                            ),
                            // validator: MinLengthValidator(6, errorText: "password should be atleast 6 characters"),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 0,
                ),
                SizedBox(
                  height: 45,
                  width: 250,
                  child: RaisedButton(
                    color: Color(0xFF1B5E20),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text(
                      'LOGIN',
                      style: TextStyle(fontSize: 25, color: Colors.white),
                    ),
                    onPressed: _emailController.text == "" ||
                            _passController.text == ""
                        ? null
                        : () {
                            setState(() {
                              _isLoading = true;
                            });
                            signIn(_emailController.text, _passController.text);
                          },
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                FlatButton(
                  onPressed: () {
                    /* Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => new NotificationPage()));*/
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => new ContactUs()));
                  },
                  child: Text(
                    "Forgot Password",
                    style: TextStyle(
                        color: Colors.blueGrey, fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showFloatingFlushbar() {
    Flushbar(
      padding: EdgeInsets.all(20),
      borderRadius: 8,
      backgroundGradient: LinearGradient(
        colors: [Colors.red.shade800, Colors.red],
        stops: [0.6, 1],
      ),
      boxShadows: [
        BoxShadow(
          color: Colors.white,
          offset: Offset(3, 3),
          blurRadius: 3,
        ),
      ],
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      forwardAnimationCurve: Curves.fastLinearToSlowEaseIn,
      title: 'Wrong Email or Password',
      message: 'Please enter Seller\'s Email and Password',
      icon: Icon(
        Icons.info_outline,
        size: 30,
        color: Colors.white,
      ),
    )..show(context);
  }
}
