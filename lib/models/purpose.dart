import 'package:flutter/material.dart';

class Purpose {
  static const String PurposeRice = 'Rice';
  static const String PurposeWheat = 'Wheat';
  static const String PurposeMaize = 'Maize';
  static const String PurposeMustard = 'Mustard';
  static const String PurposeOthers = 'Others';

  Map<String, bool> purposes = {
    PurposeRice: false,
    PurposeWheat: false,
    PurposeMaize: false,
    PurposeMustard: false,
    PurposeOthers: false,
  };
}
