import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:IMS/pages/app_main.dart';
import 'package:IMS/pages/salesinfo.dart';

class FarmerInfo extends StatefulWidget {
  @override
  _FarmerInfoState createState() => _FarmerInfoState();
}

class _FarmerInfoState extends State<FarmerInfo> {
  //Validation of farmer's form
  GlobalKey<FormState> farmerformkey = GlobalKey<FormState>();

  void farmervalidate() {
    if (farmerformkey.currentState.validate()) {
      print("Data inserted successfully");
    } else {
      print("**ERROR** Please fill up the table");
    }
  }

  String validatename(value) {
    if (value.isEmpty) {
      return "Enter name";
    } else {
      return null;
    }
  }

  String validateland(value) {
    if (value.isEmpty) {
      return "Enter land area";
    } else {
      return null;
    }
  }

  String validatephone(value) {
    if (value.isEmpty) {
      return "Enter in numbers";
    } else {
      return null;
    }
  }

  String validateid(value) {
    if (value.isEmpty) {
      return "Enter Citizen ID";
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //backgroundColor: Color.fromRGBO(97, 112, 228, 1),
        backgroundColor: Color(0xFF1B5E20),
        leading: IconButton(
          icon: Icon(
            Icons.home,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => new FirstPage()));
          },
        ),
        title: Text(
          "Farmer's Information",
          textAlign: TextAlign.center,
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 20.0,
          horizontal: 17.0,
        ),
        child: Form(
            key: farmerformkey,
            autovalidate: true,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  TextFormField(
                    validator: validatename,
                    decoration: InputDecoration(
                        labelText: 'Name',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    validator: validateland,
                    decoration: InputDecoration(
                        labelText: 'Land Area',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    validator: validatephone,
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    decoration: InputDecoration(
                        labelText: 'Phone number',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    validator: validateid,
                    decoration: InputDecoration(
                        labelText: 'Citizen ID',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Color.fromRGBO(97, 112, 228, 1),
                        ),
                        borderRadius: BorderRadius.circular(10.0)),
                    child: DropDownField(
                      labelStyle: const TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w400,
                      ),
                      labelText: 'Address',
                      controller: addressselected,
                      hintText: "Search Records",
                      enabled: true,
                      itemsVisibleInDropdown: 6,
                      items: Address,
                      onValueChanged: (value) {
                        setState(() {
                          select = value;
                        });
                      },
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    cursorColor: Color.fromRGBO(97, 112, 228, 1),
                    decoration: InputDecoration(
                        labelText: 'Ward no.',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  RaisedButton(
                    color: Color(0xFF1B5E20),
                    onPressed: farmervalidate,
                    child: Text(
                      'Insert',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                  RaisedButton(
                    //color: Color.fromRGBO(97, 112, 228, 1),
                    color: Colors.red,
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => new Home()));
                    },
                    child: Text(
                      'Back to list',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            )),
      ),
    );
  }
}

String select = "";

final addressselected = TextEditingController();
// creating a list of strings for commodities
List<String> Address = [
  "Aadarsha, Doti",
  "Aagnisaira Krishnasawaran, Saptari",
  "Aalitaal, Dadeldhura",
  "Aamachodingmo, Rasuwa",
  "Aamchok, Bhojpur",
  "Aanbu Khaireni, Tanahun",
  "Aandhikhola, Syangja",
  "Aarughat, Gorkha",
  "Aathabis, Dailekh",
  "Aathabiskot, Western Rukum",
  "Aathrai, Terhathum",
  "Aathrai Triveni, Taplejung",
  "Aaurahi, Dhanusha",
  "Aaurahi, Siraha",
  "Adanchuli,Humla",
  "Adarsha Kotwal, Bara",
  "Airawati, Pyuthan",
  "Aiselukharka, Khotang",
  "Ajaymeru, Dadeldhura",
  "Ajirkot,Gorkha",
  "Bajura,Gorkha",
  "GaidaChowk,Chitwan"
];
