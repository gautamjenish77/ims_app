import 'package:dropdownfield/dropdownfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:IMS/pages/farmerinfo.dart';
import 'package:form_field_validator/form_field_validator.dart';
import '../models/purpose.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

const MaterialColor _buttonTextColor = MaterialColor(0xFF536DFE, <int, Color>{
  50: Color(0xFF1A237E),
  100: Color(0xFF1A237E),
  200: Color(0xFF1A237E),
  300: Color(0xFF1A237E),
  400: Color(0xFF1A237E),
  500: Color(0xFF1A237E),
  600: Color(0xFF1A237E),
  700: Color(0xFF1A237E),
  800: Color(0xFF1A237E),
  900: Color(0xFF1A237E),
});

class _HomeState extends State<Home> {
  String title = 'Date Picker';
  DateTime _date = DateTime.now();

  Future<Null> _selectDate(BuildContext context) async {
    DateTime _datePicker = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: DateTime(2000),
        lastDate: DateTime(2030),
        textDirection: TextDirection.ltr,
        initialDatePickerMode: DatePickerMode.day,
        //selectableDayPredicate: (DateTime val) => val.weekday ==6 || val.weekday == 7 ? false : true,
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: ThemeData(
              primarySwatch: _buttonTextColor,
              primaryColor: Color.fromRGBO(97, 112, 228, 1),
              accentColor: Color.fromRGBO(97, 112, 228, 1),
            ),
            child: child,
          );
        });
    if (_datePicker != null && _datePicker != _date) {
      setState(() {
        _date = _datePicker;
        print(
          _date.toString(),
        );
      });
    }
  }

//validation of form starts from here

  GlobalKey<FormState> salesformkey = GlobalKey<FormState>();

  void salesvalidate() {
    if (salesformkey.currentState.validate()) {
      print("Saved successfully with Proper Validation");
    } else {
      print("Error 'Please Try Again'");
    }
  }

  String validatequantity(value) {
    if (value.isEmpty) {
      return "Enter Quantity in numbers";
    } else {
      return null;
    }
  }

  /*String validateprice(value) {
    if (value.isEmpty) {
      return "Enter Price in numbers";
    } else {
      return null;
    }
  }*/

  String validatefarmer(value) {
    if (value.isEmpty) {
      return "Enter name";
    } else {
      return null;
    }
  }

  //CHECKBOX PART

  final _purpose = Purpose();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //backgroundColor: Color.fromRGBO(97, 112, 228, 1),
        backgroundColor: Color(0xFF1B5E20),
        title: Text(
          "Sales Information",
          textAlign: TextAlign.center,
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 20.0,
          horizontal: 27.0,
        ),
        child: Form(
            key: salesformkey,
            autovalidate: true,
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                        border: Border.all(
                          color: Color.fromRGBO(97, 112, 228, 1),
                        ),
                        borderRadius: BorderRadius.circular(10.0)),
                    child: DropDownField(
                      labelStyle: const TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w400,
                      ),
                      labelText: 'Commodity',
                      controller: comodityselected,
                      hintText: "Choose one",
                      enabled: true,
                      itemsVisibleInDropdown: 4,
                      items: Commodity,
                      onValueChanged: (value) {
                        setState(() {
                          choose = value;
                        });
                      },
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    cursorColor: Color.fromRGBO(97, 112, 228, 1),
                    decoration: InputDecoration(
                        labelText: 'Quantity in MT(Metric Ton)',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                    validator: validatequantity,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    inputFormatters: [
                      WhitelistingTextInputFormatter.digitsOnly
                    ],
                    cursorColor: Color.fromRGBO(97, 112, 228, 1),
                    decoration: InputDecoration(
                        labelText: 'Bag',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                    validator: validatequantity,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  /* TextFormField(
                inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
                cursorColor: Colors.indigo,
                validator: validateprice,
                decoration: InputDecoration(
                    labelText: 'Price',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    )),
              ),*/
                  Container(
                    child: Text(
                      'Purpose',
                      style: TextStyle(
                        color: Colors.indigo,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    color: Colors.lightGreen.withOpacity(.2),
                    child: CheckboxListTile(
                      activeColor: Colors.indigo,
                      title: const Text('Rice'),
                      value: _purpose.purposes[Purpose.PurposeRice],
                      onChanged: (val) {
                        setState(
                            () => _purpose.purposes[Purpose.PurposeRice] = val);
                      },
                    ),
                  ),
                  Container(
                    color: Colors.lightGreen.withOpacity(.2),
                    child: CheckboxListTile(
                      activeColor: Colors.indigo,
                      title: const Text('Wheat'),
                      value: _purpose.purposes[Purpose.PurposeWheat],
                      onChanged: (val) {
                        setState(() =>
                            _purpose.purposes[Purpose.PurposeWheat] = val);
                      },
                    ),
                  ),
                  Container(
                    color: Colors.lightGreen.withOpacity(.2),
                    child: CheckboxListTile(
                      activeColor: Colors.indigo,
                      title: const Text('Maize'),
                      value: _purpose.purposes[Purpose.PurposeMaize],
                      onChanged: (val) {
                        setState(() =>
                            _purpose.purposes[Purpose.PurposeMaize] = val);
                      },
                    ),
                  ),
                  Container(
                    color: Colors.lightGreen.withOpacity(.2),
                    child: CheckboxListTile(
                      activeColor: Colors.indigo,
                      title: const Text('Mustard'),
                      value: _purpose.purposes[Purpose.PurposeMustard],
                      onChanged: (val) {
                        setState(() =>
                            _purpose.purposes[Purpose.PurposeMustard] = val);
                      },
                    ),
                  ),
                  Container(
                    color: Colors.lightGreen.withOpacity(.2),
                    child: CheckboxListTile(
                      activeColor: Colors.indigo,
                      title: const Text('Others'),
                      value: _purpose.purposes[Purpose.PurposeOthers],
                      onChanged: (val) {
                        setState(() =>
                            _purpose.purposes[Purpose.PurposeOthers] = val);
                      },
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    cursorColor: Colors.indigo,
                    validator: validatefarmer,
                    decoration: InputDecoration(
                        labelText: 'Farmer',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    cursorColor: Colors.indigo,
                    validator: (String value) {
                      if (value.isEmpty) {
                        return 'Fill it';
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                        labelText: 'Remarks',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  TextFormField(
                    cursorColor: Colors.indigo,
                    onTap: () {
                      setState(() {
                        _selectDate(context);
                      });
                    },
                    decoration: InputDecoration(
                        labelText: 'Date',
                        hintText: (_date.toString()),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        )),
                  ),
                  RaisedButton(
                    color: Color(0xFF1B5E20),
                    onPressed: salesvalidate,
                    child: Text(
                      'Save Changes',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  RaisedButton(
                    color: Colors.red,
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => new FarmerInfo()));
                    },
                    child: Text(
                      'Farmer Login',
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            )),
      ),
    );
  }
}

save() {
  print('Saving sales using a web services');
}

String choose = "";

final comodityselected = TextEditingController();
// creating a list of strings for commodities
List<String> Commodity = [
  "Urea",
  "DAP",
  "Potash",
  "Chun",
];
