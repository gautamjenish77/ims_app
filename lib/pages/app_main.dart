import 'dart:async';

import 'package:IMS/Logincomponents/newLogin_page.dart';
import 'package:IMS/addData.dart';
import 'package:flutter/material.dart';
import 'package:IMS/pages/notification.dart';
import 'package:IMS/pages/salesinfo.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:connectivity/connectivity.dart';
import 'package:webview_flutter/webview_flutter.dart';

class FirstPage extends StatefulWidget {
  @override
  _FirstPageState createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  String result = '';
  var Colorsval = Colors.white;

  @override
  void initState() {
    CheckStatus();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //backgroundColor: Color.fromRGBO(97, 112, 228, 1),
        backgroundColor: Color(0xFF1B5E20),
        //   leading: Column(
        //   children: <Widget>[],
        //),
        title: Center(
          child: Text(
            "Shiva Shakti B.U.s.s.s",
            textAlign: TextAlign.start,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 18.0,
            ),
          ),
        ),

        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.notifications,
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => new NotificationPage()));
            },
          ),
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Image.asset(
                'assets/blue.png',
                height: 10.0,
                width: 20.0,
                fit: BoxFit.fill,
              ),
            ),
            /* InkWell(
              onTap: () {},
              child: ListTile(
                title: Text(
                  'Purchase',
                  style: TextStyle(color: Colors.black, fontSize: 16.0),
                ),
                leading: Icon(
                  Icons.insert_chart,
                  color: Colors.blue,
                  size: 25.0,
                ),
              ),
            ),*/
            InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new Home()));
              },
              child: ListTile(
                title: Text(
                  'Add new Sales',
                  style: TextStyle(color: Colors.black, fontSize: 16.0),
                ),
                leading: Icon(
                  Icons.send,
                  color: Colors.red,
                  size: 25.0,
                ),
              ),
            ),
            Divider(
              color: Colors.black38,
            ),
            InkWell(
              onTap: () {},
              child: ListTile(
                title: Text(
                  'Goods',
                  style: TextStyle(color: Colors.black, fontSize: 16.0),
                ),
                leading: Icon(
                  Icons.account_balance,
                  color: Colors.deepPurple,
                  size: 25.0,
                ),
              ),
            ),
            Divider(
              color: Colors.black38,
            ),
            InkWell(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => new LoginPage1()));
              },
              child: ListTile(
                title: Text(
                  'Log Out',
                  style: TextStyle(color: Colors.black, fontSize: 16.0),
                ),
                leading: Icon(
                  Icons.assignment_return,
                  color: Color.fromRGBO(97, 112, 228, 1),
                  size: 25.0,
                ),
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(0.0),
          child: Column(
            children: <Widget>[
              Positioned(
                left: 0.0,
                right: 0.0,
                height: 25.0,
                child: Container(
                  color: Colorsval,
                  height: 27.0,
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            result != null ? result : "Unknown",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Welcome, Rara IT !",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "This is your Dashboard. Here you can see notices and instructions to manage this application. Check this regularly for updated content, notices, instructions and new widgets.",
                  textAlign: TextAlign.justify,
                  style: TextStyle(
                      color: Colors.blueGrey,
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(2),
                  border: Border.all(color: Colors.black12),
                ),
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Example Notice :",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "This is to inform all our users that as per lockdown imposed by the Government of Nepal, all the Distribution will be suspended till 31 June 2020. As per alarming situation related to the spreading of Covid-19,",
                          textAlign: TextAlign.justify,
                          style:
                              TextStyle(color: Colors.blueGrey, fontSize: 15.0),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              Container(
                padding: EdgeInsets.all(15),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(2),
                  border: Border.all(color: Colors.black12),
                ),
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topLeft,
                        child: Text(
                          "Application User Guide :",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "Purchase : All purchase records can be found in this section.You can Add, Update and Delete your purchase records here as the purchase occours.",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              color: Colors.blueGrey,
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "Sales : All sales records can be found in this section. You can Add, Update and Delete your sales records here as the sale occours.",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              color: Colors.blueGrey,
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "Distribution Center : Manage distributor entity that you own. You can Manage your entity. Manage, Filter and see the purchase/sales report.",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              color: Colors.blueGrey,
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "Report : Generate purchase and sales report. Filter by different fiscal year.",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              color: Colors.blueGrey,
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "User Management : Manage all users whose have access to your Distribution Center. You can also create new users as required.",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              color: Colors.blueGrey,
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => AddData()));
        },
        child: Icon(Icons.add),
        backgroundColor: Colors.green,
      ),
    );
  }

  void CheckStatus() {
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      if (result == ConnectivityResult.mobile ||
          result == ConnectivityResult.wifi) {
        ChangeValues("Online", Colors.green[900]);
      } else {
        ChangeValues("Offline", Colors.red);
      }
    });
  }

  void ChangeValues(String resultval, Color colorval) {
    setState(() {
      result = resultval;
      Colorsval = colorval;
    });
  }
}

/*class ImsWebView extends StatelessWidget {
  ImsWebView(this.url);
  final String url;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Web Page"),
      ),
      body: WebView(
        initialUrl: "http://ims.rarait.com/sales/create",
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}*/
