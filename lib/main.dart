import 'package:IMS/Logincomponents/newLogin_page.dart';
import 'package:flutter/material.dart';
import 'package:IMS/animation/fadeanimation.dart';
import 'package:IMS/style.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter_offline/flutter_offline.dart';
import 'package:IMS/Logincomponents/newLogin_page.dart';
import 'package:IMS/addData.dart';

final String title = '';

void main() {
  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    home: MyApp(),
    routes: <String, WidgetBuilder>{
      '/adddata': (BuildContext context) => new AddData(title: title),
      //'/login': (BuildContext context) => new LoginPage(title: title),
    },
    theme: ThemeData(
      appBarTheme: AppBarTheme(
        // ignore: deprecated_member_use
        textTheme: TextTheme(title: AppBarTextStyle),
      ),
      textTheme: TextTheme(
        // ignore: deprecated_member_use
        title: TitleTextStyle,
        // ignore: deprecated_member_use
        body1: Body1TextStyle,
      ),
    ),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with TickerProviderStateMixin {
  AnimationController _scaleController;
  AnimationController _scale2Controller;
  AnimationController _widthController;
  AnimationController _positionController;

  Animation<double> _scaleAnimation;
  Animation<double> _scale2Animation;
  Animation<double> _widthAnimation;
  Animation<double> _positionAnimation;

  bool hideIcon = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _scaleController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 100));
    _scaleAnimation =
        Tween<double>(begin: 1.0, end: 0.8).animate(_scaleController)
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              _widthController.forward();
            }
          });
    _widthController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 400));
    _widthAnimation =
        Tween<double>(begin: 80.0, end: 300.0).animate(_widthController)
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              _positionController.forward();
            }
          });
    _positionController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _positionAnimation =
        Tween<double>(begin: 0.0, end: 215.0).animate(_positionController)
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              setState(() {
                hideIcon = true;
              });
              _scale2Controller.forward();
            }
          });
    _scale2Controller =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    _scale2Animation =
        Tween<double>(begin: 1.0, end: 32.0).animate(_scale2Controller)
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              Navigator.push(
                  context,
                  PageTransition(
                      type: PageTransitionType.fade, child: LoginPage1()));
            }
          });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        width: double.infinity,
        child: Stack(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(30.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  FadeAnimation(
                    1,
                    Center(child: Image.asset("assets/blue.png")),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  FadeAnimation(
                      1.3,
                      Center(
                        child: Text(
                          "IMS presents",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              //color: Color.fromRGBO(97, 112, 228, 1),
                              color: Color(0xFF1B5E20),
                              height: 1.4,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      )),
                  FadeAnimation(
                      1.6,
                      Center(
                        child: Text(
                          "Fertilizer Management System",
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            //color: Color.fromRGBO(97, 112, 228, 1),
                            color: Color(0xFF1B5E20),
                            height: 1.4,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      )),
                  SizedBox(
                    height: 200,
                  ),
                  FadeAnimation(
                      1.9,
                      AnimatedBuilder(
                        animation: _scaleController,
                        builder: (context, child) => Transform.scale(
                            scale: _scaleAnimation.value,
                            child: Center(
                              child: AnimatedBuilder(
                                animation: _widthController,
                                builder: (context, child) => Container(
                                  width: _widthAnimation.value,
                                  height: 80,
                                  padding: EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(50),
                                      color: Color(0xFF1B5E20).withOpacity(.4)),
                                  child: InkWell(
                                    onTap: () {
                                      _scaleController.forward();
                                    },
                                    child: Stack(children: <Widget>[
                                      AnimatedBuilder(
                                        animation: _positionController,
                                        builder: (context, child) => Positioned(
                                          left: _positionAnimation.value,
                                          child: AnimatedBuilder(
                                            animation: _scale2Controller,
                                            builder: (context, child) =>
                                                Transform.scale(
                                                    scale:
                                                        _scale2Animation.value,
                                                    child: Container(
                                                      width: 60,
                                                      height: 60,
                                                      decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color:
                                                            Color(0xFF1B5E20),
                                                      ),
                                                      child: hideIcon == false
                                                          ? Icon(
                                                              Icons
                                                                  .arrow_forward,
                                                              color:
                                                                  Colors.white,
                                                            )
                                                          : Container(),
                                                    )),
                                          ),
                                        ),
                                      ),
                                    ]),
                                  ),
                                ),
                              ),
                            )),
                      )),
                  SizedBox(
                    height: 60,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
